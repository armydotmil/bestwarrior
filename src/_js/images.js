/*global $,document,Fuse,Spinner*/

function Images() {
    var opts = {
        lines: 13, // The number of lines to draw
        length: 20, // The length of each line
        width: 10, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        color: '#000', // #rgb or #rrggbb or array of colors
        className: 'spin', // The CSS class to assign to the spinner
        zIndex: 2, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };

    // refer to the spinner with: target.append(spinner.el);
    // then stop with spinner.stop();
    this.spinner = new Spinner(opts).spin();

    $('.spinner').show().append(this.spinner.el);

    this.images_data = [];
    this.clear = false;
    this.init();
}

Images.prototype.init = function() {
    var $this = this, i = 0;

    $.ajax({
        url: 'http://www.army.mil/api/packages/getpackagesbykeywords',
        type: 'get',
        data: 'keywords=target_bestwarrior&count=100&limit=1',
        dataType: 'json',
        success: function(data) {
            if (data) {
                $this.spinner.stop();
                $('.spinner').hide();
                for (i; i < data.length; i++) {
                    data[i].pos = i;
                    $this.images_data.push(data[i]);
                }
                $this.build_images($this.images_data);
            }
        },

        error: function(jqXHR, textStatus) {
            $this.spinner.stop();
            $('.spinner').hide();
            $('.images').append(
                '<p>Error retrieving images: ' + textStatus + '</p>'
            );
        }
    });
};

Images.prototype.build_images = function(items) {
    var date, date_p, date_parts, download, header, i = 0, image, j, link,
    map = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ],
    title;

    for (i; i < items.length; i++) {
        for (j = 0; j < items[i].images.length; j++) {
            image = items[i].images[j];

            date_parts = '';
            date = '';
            if (image.date !== undefined) {
                date_parts = image.date.substring(0, 10).split('-');
                date = map[parseInt(date_parts[1], 10) - 1] +
                    ' ' + date_parts[2] + ', ' + date_parts[0];
            }

            title = image.title;
            if (title !== undefined && title.length > 50) {
                title = title.substring(0, 50) + '...';
            }

            link = $('<a>').prop({
                'href': image.page_url,
                'target': '_blank'
            }).append(
                $('<img>').prop({
                    'src': image.url_size3,
                    'alt': image.alt
                })
            );
            header = $('<h4>').append(
                $('<a>').prop({
                    'href': image.page_url,
                    'target': '_blank'
                }).text(title)
            );
            download = $('<p>').append(
                $('<a>').prop({
                    'href': image.url,
                    'target': '_blank'
                }).text('Download')
            );
            date_p = $('<p>').text(date);

            $('.images ul').append(
                $('<li class="img-block group' + i + '">').append(
                    link, header, download, date_p
                )
            );
        }
    }
};

Images.prototype.display = function(items) {
    var blocks = $('.images ul li'), i = 0;

    // first hide all image blocks
    $(blocks).hide();

    // then show results by class name based on pos
    for (i; i < items.length; i++) {
        $(blocks).filter('.group' + items[i].pos).show();
    }
};

Images.prototype.token_exist = function(token, keywords) {
    var patt;
    try {
        patt = new RegExp(token, 'gi');
        return (patt.exec(keywords)) ? 1 : 0;
    }
    catch (e) {
        return false;
    }
};

Images.prototype.fuse_search = function(token) {
    var fuse, i = 0, list = [],
    options, result;

    for (i; i < this.images_data.length; i++) {
        if (!this.images_data[i].hits) {
            list.push(this.images_data[i]);
        }
    }

    options = {
        caseSensitive: false,
        includeScore: false,
        shouldSort: true,
        threshold: 0.4,
        location: 0,
        distance: 100,
        maxPatternLength: 50,
        keys: [
            'description',
            'keywords',
            'title',
            'images.title',
            'images.keywords'
        ]
    };
    fuse = new Fuse(list, options);
    result = fuse.search(token);

    return result;
};

Images.prototype.perform_search = function(tokens) {
    var fuse_results = [], hits, i = 0, images, j = 0,
    x = 0, results = [];

    for (i; i < this.images_data.length; i++) {
        hits = 0;
        images = this.images_data[i].images;

        hits += this.token_exist(tokens, this.images_data[i].description);
        hits += this.token_exist(tokens, this.images_data[i].title);
        hits += this.token_exist(tokens, this.images_data[i].keywords);

        for (j; j < images.length; j++) {
            hits += this.token_exist(tokens, images[j].title);
            hits += this.token_exist(tokens, images[j].keywords);
        }

        this.images_data[i].hits = hits;
        if (hits) results.push(this.images_data[i]);
    }

    // now do fuse search
    fuse_results = this.fuse_search(tokens);

    for (x; x < fuse_results.length; x++) {
        results.push(fuse_results[x]);
    }

    // sort results at the end (by relevance)
    this.sort_results(results);
    this.results = results.slice(0);
};

Images.prototype.reset_hits = function() {
    var i = 0;

    for (i; i < this.images_data; i++) {
        this.images_data[i].hits = 0;
    }
};

Images.prototype.sort_results = function(results) {
    return results.sort(function(a, b) {
        var x = (a.hits) ? a.hits : 0;
        var y = (b.hits) ? b.hits : 0;
        return ((x < y) ? 1 : ((x > y) ? -1 : 0));
    });
};

Images.prototype.get_results = function() {
    return this.results;
};

Images.prototype.show_query = function(query) {
    if (query !== '') {
        $('.show-query').show().find('span').html(query);
    } else {
        $('.show-query').hide();
    }
};

Images.prototype.search = function() {
    var query = $('#query').val(), search_results;

    if (query !== '') {
        $('.search-button').addClass('clear');
        this.clear = true;

        // do the search
        this.perform_search(query);

        // get the results
        search_results = this.get_results();
    } else {
        $('.search-button').removeClass('clear');
        this.clear = false;

        // assume we want to reset the search
        this.reset_hits();
        search_results = this.images_data;
    }

    // show the user what they are searching for
    this.show_query(query);

    // show search results
    this.display(search_results);
};

$(document).ready(function() {
    var images = new Images();

    $('#query').on('focus', function() {
        $(this).parent().addClass('focused');
    }).on('blur', function() {
        $(this).parent().removeClass('focused');
    });

    // bind submit event for search
    $('#search').on('submit', function(e) {

        // don't do the normal form submit thing
        e.preventDefault();

        images.search();
    });

    $('.search-button').on('click', function() {
        if (images.clear) $('#query').val('');
        images.search();
    });
});
